class ShippingRate < ActiveRecord::Base
  belongs_to :country
  belongs_to :currency

  DEFAULT_RATE = "AUD"

  scope :old_rate, -> { find_by(currency: Currency.find_by_code(DEFAULT_RATE)) }
  scope :find_by_currency, -> (code) { find_by(currency: Currency.find_by(code: code)) }


  def json
    {self.currency.code => { "regular" => self.regular, "express" => self.express }}
  end

  def currency_json
    { "regular" => self.regular, "express" => self.express }
  end


end
