class Country < ActiveRecord::Base

  has_many :shipping_rates
  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true

  def merge_shipping_rates
    self.shipping_rates.inject(Hash.new()){|memo, sp| memo.merge!(sp.json); memo}
  end


end
