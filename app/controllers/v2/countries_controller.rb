module V2
  class CountriesController < ApplicationController

    before_action :find_country_by_code, only: :shipping_rates

    def shipping_rates
      if(shipping_rates_params[:currency].nil?)
        render :json => @country.merge_shipping_rates
      else
        currency = shipping_rates_params[:currency]
        shipping_rate = @country.shipping_rates.find_by_currency(currency)
        render :json => shipping_rate.currency_json
      end
    end

    private

    def find_country_by_code
      @country = Country.find_by_code(shipping_rates_params[:country_code])
      if @country.nil?
        render :json => {  :message => "Resource not found" }, :status => 404
      end
    end

    def shipping_rates_params
      params.permit(:country_code, :currency)
    end

  end
end
