module V1
  class CountriesController < ApplicationController

    before_action :find_country_by_code, only: :shipping_rates

    def shipping_rates
      shipping_rate = @country.shipping_rates.old_rate
      if shipping_rate.blank?
        render_not_found
      else
        render :json => shipping_rate.currency_json
      end
    end

    private

    def find_country_by_code
      @country = Country.find_by_code(shipping_rates_params[:country_code])
      render_not_found if @country.nil?
    end

    def render_not_found
      render :json => { :message => "Resource not found" }, :status => 404
    end

    def shipping_rates_params
      params.permit(:country_code, :currency)
    end

  end
end
