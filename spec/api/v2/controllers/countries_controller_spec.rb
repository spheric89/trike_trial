require "rails_helper"
describe V2::CountriesController do

  describe "shipping_rates" do

    let(:accept_headers) { "version=2"}
    let(:currencies) {["AUD", "USD", "EUR"]}
    let!(:create_currencies) { currencies.each {|code| create(:currency, :code => code)} }
    let!(:country) { create :country, :code => "AU" }
    before(:each) { create :country, :code => "US" }
    let!(:create_shipping_rates) { currencies.each {|code| create(:shipping_rate, country: country, currency: Currency.find_by_code(code))} }

    context "when request is valid" do
      subject(:shipping_rates) { get "countries/shipping_rates", {:country_code => "AU", :format => :json}, {'HTTP_ACCEPT' => accept_headers} }
      it { is_expected.to eq(200) }

      context "when no currency code is provided" do 
        describe "response body" do 
          subject(:response_body) { shipping_rates; JSON.parse(response.body) }

          specify { expect(response_body).to eq country.merge_shipping_rates }
        end
      end

      context "when a currency is provided" do 
        let(:currency){"AUD"}
        subject(:shipping_rates) { get "countries/shipping_rates", {:country_code => "AU", :currency => currency, :format => :json}, {'HTTP_ACCEPT' => accept_headers} }
        describe "response body" do 
          subject(:response_body) { shipping_rates; JSON.parse(response.body) }
          specify { expect(response_body).to eq country.shipping_rates.find_by_currency(currency).currency_json }
        end
      end
    end

    context "when no country code is provided" do
      subject(:shipping_rates) { get "countries/shipping_rates", {:format => :json}, {'HTTP_ACCEPT' => accept_headers} }
      it { is_expected.to eq(404) }
    end

    context "when request is invalid" do
      describe "no version in accept header" do 
        subject(:shipping_rates) { get "countries/shipping_rates", {:country_code => "AU", :format => :json}}
        it  { expect{subject}.to raise_error(ActionController::RoutingError) }
      end
    end

  end

end
