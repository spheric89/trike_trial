FactoryGirl.define do

  factory :shipping_rate do
    regular { (rand(100) + 10).to_s }
    express { (rand(100) + 10).to_s }
    country
    currency
  end

end
