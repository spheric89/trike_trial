FactoryGirl.define do

  factory :currency do
    code { ["AUD", "EUR", "USD"].sample }
  end

end
