class CreateShippingRates < ActiveRecord::Migration
  def change
    create_table :shipping_rates do |t|
      t.integer :country_id
      t.integer :currency_id
      t.string :regular
      t.string :express
      t.index :country_id
      t.index :currency_id

      t.timestamps
    end
  end
end
