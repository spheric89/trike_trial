class ApiConstraint
  attr_reader :v

  def initialize(v)
    @v = v.fetch(:version)
  end

  def matches?(request)
    request.headers.fetch(:accept).include?("version=#{v}")
  end

end

Rails.application.routes.draw do
  scope module: :v1, constraints: ApiConstraint.new(version: 1) do
    resources :countries, :only => [] do
      get :shipping_rates, :on => :collection
    end
  end

  scope module: :v2, constraints: ApiConstraint.new(version: 2) do
    resources :countries, :only => [] do
      get :shipping_rates, :on => :collection
    end
  end
end
